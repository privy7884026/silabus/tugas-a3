package main

import (
	"log"
	"pustaka-api/book"
	"pustaka-api/helper"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	dsn := "root:@tcp(127.0.0.1:3306)/pustaka-api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("DB Not Conect")
	}

	db.AutoMigrate(&book.Book{})

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := helper.NewBookHandler(bookService)

	router := gin.Default()

	router.GET("/status", bookHandler.StatusHandler)
	router.GET("/books", bookHandler.GetAllBook)
	router.GET("/books/:id", bookHandler.GetDetailBook)
	router.POST("/books", bookHandler.PostBook)
	router.PATCH("/books/:id", bookHandler.UpdateBook)
	router.DELETE("/books/:id", bookHandler.DeleteBook)

	router.Run()
	// router.Run(":6666") custom port
}
