package helper

import (
	"fmt"
	"net/http"
	"pustaka-api/book"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type bookHandler struct {
	bookService book.Service
}

func NewBookHandler(bookservice book.Service) *bookHandler {
	return &bookHandler{bookservice}
}

func (h *bookHandler) StatusHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"Status": "Ok",
	})
}

func (h *bookHandler) GetAllBook(c *gin.Context) {
	books, err := h.bookService.FindAll()
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"Error": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"List of books": books,
	})
}

func (h *bookHandler) GetDetailBook(c *gin.Context) {
	idString := c.Param("id") // menangkap id

	id, _ := strconv.Atoi(idString)

	books, err := h.bookService.FindID(int(id))
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"Error": err,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"Detail of books with id": idString,
		"Detail book":             books,
	})
}

func (h *bookHandler) PostBook(c *gin.Context) {
	var bookRequest book.BookRequest
	err := c.ShouldBindJSON(&bookRequest)

	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Erorr on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": errorMessages,
		})
		return
	}

	book, err := h.bookService.Create(bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
	})
}

func (h *bookHandler) UpdateBook(c *gin.Context) {
	var bookRequest book.BookRequest
	err := c.ShouldBindJSON(&bookRequest)

	if err != nil {
		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Erorr on field %s, condition: %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": errorMessages,
		})
		return
	}
	idString := c.Param("id") // menangkap id
	id, _ := strconv.Atoi(idString)
	book, err := h.bookService.Update(id, bookRequest)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"data": book,
	})
}

func (h *bookHandler) DeleteBook(c *gin.Context) {
	idString := c.Param("id") // menangkap id
	id, _ := strconv.Atoi(idString)
	book, err := h.bookService.Delete(id)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"Error": err,
		})
	}

	c.JSON(http.StatusOK, gin.H{
		"ID":     book.ID,
		"Status": "Berhasil Dihapus",
	})
}
