package book

type Service interface {
	FindAll() ([]Book, error)
	FindID(ID int) (Book, error)
	Create(bookrequest BookRequest) (Book, error)
	Update(ID int, bookrequest BookRequest) (Book, error)
	Delete(ID int) (Book, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindAll() ([]Book, error) {
	// return s.repository.FindAll()
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) FindID(ID int) (Book, error) {
	book, err := s.repository.FindID(ID)
	return book, err
}

func (s *service) Create(bookrequest BookRequest) (Book, error) {
	book := Book{
		Title:       bookrequest.Title,
		Description: bookrequest.Description,
		Price:       bookrequest.Price,
		Rating:      bookrequest.Rating,
		Discount:    bookrequest.Discount,
	}
	newbook, err := s.repository.Create(book)
	return newbook, err
}

func (s *service) Update(ID int, bookrequest BookRequest) (Book, error) {
	book, err := s.repository.FindID(ID)

	book.Title = bookrequest.Title
	book.Description = bookrequest.Description
	book.Price = bookrequest.Price
	book.Rating = bookrequest.Rating
	book.Discount = bookrequest.Discount

	newbook, err := s.repository.Update(book)
	return newbook, err
}

func (s *service) Delete(ID int) (Book, error) {
	book, err := s.repository.FindID(ID)
	newbook, err := s.repository.Delete(book)
	return newbook, err
}
