package book

type BookRequest struct {
	// ID          uint
	Title       string `json:"title" binding:"required"`
	Description string `json:"description" binding:"required"`
	Price       int    `json:"price" binding:"required,number"`
	Rating      int    `json:"rating" binding:"required,number"`
	Discount    int    `json:"discount" binding:"required,number"`
	// CreatedAt   time.Time
	// UpdatedAt   time.Time
}
