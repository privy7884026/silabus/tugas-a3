package main

import (
	"context"
	"log"
	"mongodb/usecase"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// var mongoClient *mongo.Client

// func init() {
// 	//load env

// 	err := godotenv.Load()
// 	if err != nil {
// 		log.Fatal("error load env", err)
// 	}
// 	log.Println("env loaded")

// 	mongoClient, err := mongo.Connect(context.Background(), options.Client().ApplyURI(os.Getenv("MONGO_URL")))
// 	if err != nil {
// 		log.Fatal("eror", err)
// 	}
// 	err = mongoClient.Ping(context.Background(), readpref.Primary())
// 	if err != nil {
// 		log.Fatal("eror", err)
// 	}
// 	log.Println("Mongo Connect")
// }

var collection *mongo.Collection
var ctx = context.TODO()

func init() {
	clientOptions := options.Client().ApplyURI("mongodb+srv://admin:admin@privy.caxfrbi.mongodb.net/?retryWrites=true&w=majority&appName=Privy")
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database(os.Getenv("DB_NAME")).Collection(os.Getenv("COLLECTION_NAME"))
}

func main() {
	//close conection
	//defer mongoClient.Disconnect(context.Background())
	//coll := mongoClient.Database(os.Getenv("DB_NAME")).Collection(os.Getenv("COLLECTION_NAME"))
	empService := usecase.EmployeeService{MongoCollection: collection}

	r := mux.NewRouter()

	r.HandleFunc("/health", healthHandler).Methods(http.MethodGet)

	r.HandleFunc("/employee", empService.CreateEmployee).Methods(http.MethodPost)
	r.HandleFunc("/employee/{id}", empService.GetEmployeeByID).Methods(http.MethodGet)
	r.HandleFunc("/employee", empService.GetAllEmployee).Methods(http.MethodGet)
	r.HandleFunc("/employee/{id}", empService.UpdateEmployeeByID).Methods(http.MethodPut)
	r.HandleFunc("/employee/{id}", empService.DeleteEmployeeByID).Methods(http.MethodDelete)
	r.HandleFunc("/employee", empService.DeleteAllEmployee).Methods(http.MethodDelete)

	log.Println("listening on port 4444")
	http.ListenAndServe(":4444", r)

}

func healthHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("runing......"))
}
